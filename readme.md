#Api

## Things to do

* Create database schema - 
* Create Factories -
* Create Seeders -
* Create Models - 
* Create a console command to import data -
* API endpoint to ADD orders - 
* API endpoint to REFUND orders -
* Reporting:
  * Total Nu of transactions -
  * Revenue in a given area/location -
  

### Nice to have

* Caching
* Versioning 
* API Docs

## Notes on my thought process

I have left little // TODO with my thoughts and here's some more:

* Assuming every time the Orders come in from batch - they will be completed.
* Normally I would alter the original table when making changes (during intial development phase)
but will create migrations instead..
* Normally I find adding Repositories/Services when most times you only do things at one place to be counter productive. it takes same
amount of time doing it now vs later.
* I am not sure what the following means but I will take it to mean 'Total number' ie count of how many transactions and regardless of status (refund/completed)

   ```Total number of transactions in a given location/area in a given day```
   
   
## Installation guide

### Packages

* composer (Read: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-14-04)
* Server (Valet, Homestead, Wamp/Lamp) (Read: https://laravel.com/docs/5.6/installation)
* Postman (or similar) to work with API. (See: https://www.getpostman.com/)

### Google API

* Register for a google account 
* Go here: https://console.developers.google.com/apis/library?project=thematic-flash-127316&authuser=1&folder&organizationId 
* Search for Geocode and click on the one on left (Geocoding Api)
* Click on enable (It may ask you to create a project around these steps)
* Once Enabled Please click on credentials tab, click on create credentials & choose API
* Note down the API KEY 

### Mail Server

* Use any of the mail settings you require - I am using mailgun (which offers free emails)
* Don't forget to add a from address in your .env

### Site


#### First steps

* clone this Repository 
* run composer install 
* start up your server & note down the URL given
* setup a new database locally & get the credentials
* copy .env.example to .env filling in required details including db & url 
* copy and paste the credentials for google api (from previous section )to your .env GOOGLE_GEOCODE_API_KEY=

#### Database 

* Please run ```php artisan migrate --seed``` to have some dummy data

#### Tests

* Please run ```phpunit --testsuite Integration``` to run all tests. hopefully they should all pass
* For Unit tests just run ```phpunit``` or ```phpunit --testsuite Unit```

#### Viewing the App

* On Postman or similar navigate to the routes (found in routes/api.php) & you are good to go

#### Running the Scheduler

* You need to add the following to your cron job (replacing path with actual path) 

```* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1```

* Or you can set a cron job directly at your server (using Forge or manually)

```* 4 * * * php /path-to-your-project/artisan system:batch-import```


## Timings

* sat 5th May - 12pm - 2.19pm
* sat 5th May - 6pm - 9pm
* sun 6th May - 12.35pm - 1.47pm
* sun 6th May - 2.05pm - 3.27pm
* sun 6th May - 5:40pm - 7.50pm
* sun 7th May - 11:14am - 12:50pm
