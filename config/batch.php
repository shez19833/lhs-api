<?php

return [
    'file_path' => 'batches/' . \Carbon\Carbon::now()->startOfDay()->addHours(3) . '.csv',
];
