<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('transactions', 'TransactionController@store');
Route::delete('transactions/{id}/refund', 'TransactionRefundController@store');

Route::get('location/transactions/count', 'TransactionCountController@index');
Route::get('location/revenue', 'LocationRevenueController@index');
