<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Repositories\TransactionRepository;

class ImportBatchTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:batch-import
                            {file_path?}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Batch Import Transactions';

    private $file_path = '';
    private $repository;

    /**
     * Create a new command instance.
     *
     * @param TransactionRepository $repository
     */
    public function __construct(TransactionRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->file_path = $this->argument('file_path') ?? config('batch.file_path');

        if ( ! Storage::exists($this->file_path) )
        {
            \Log::error('Couldn\'t find a file to import from');
            $this->error('No FILE! Exiting');
            exit();
        }

        $contents = $this->readFile($this->file_path);

        $line = 0;
        foreach ($contents as $content) {
            if ( $line == 0 ) {
                $line++;
                continue;
            }

            try {
                $this->repository->create([
                    'id' => $content[1],
                    'store_id' => $content[0],
                    'amount' => $content[2],
                    'currency' => $content[3],
                    'created_at' => Carbon::parse(strtotime($content[4]))->format('Y-m-d H:i:s'),
                ]);
            } catch (\Exception $e) {
                $error = sprintf('file: %s has an error on line %d : %s', $this->file_path, $line, $e->getMessage());
                \Log::error($error);
                $this->error($error);
            }
            $line++;
        }
    }

    private function readFile($file_path)
    {
        $handle = fopen(Storage::path($file_path), 'r');

        while (!feof($handle)) {
            yield(fgetcsv($handle));
        }

        fclose($handle);
    }
}
