<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const COMPLETED = 1;
    const REFUNDED = 2;

    public $fillable = [
        'name',
    ];

    public $table = 'status';
    public $timestamps = false;
}
