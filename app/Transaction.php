<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $dates = [
        'created_at',
    ];

    public $fillable = [
        'id',
        'store_id',
        'amount',
        'currency',
        'created_at',
    ];

    public $incrementing = false;
    public $timestamps = false;

    public function status()
    {
        return $this->hasOne(TransactionHistory::class)->orderBy('id', 'desc');
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            $model->amount = $model->amount * 100;
            $model->currency = strtoupper($model->currency);
        });

        static::created(function($model)
        {
            TransactionHistory::create([
                'transaction_id' => $model->id,
                'status_id' => Status::COMPLETED,
                'created_at' => $model->created_at,
            ]);
        });
    }
}
