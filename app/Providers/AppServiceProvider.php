<?php

namespace App\Providers;

use App\Services\Geocoding\GeocodeInterface;
use App\Services\Geocoding\Google;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(GeocodeInterface::class, Google::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
