<?php

namespace App\Services\Geocoding;

interface GeocodeInterface {

    public function getBoundaries($location);
    public function getCoordinates($location);
}