<?php

namespace App\Services\Geocoding;

use GuzzleHttp\Client;

class Google implements GeocodeInterface {

    private $client;
    private $url = 'https://maps.googleapis.com/maps/api/geocode/json?';

    public function getBoundaries($location)
    {
        $result = $this->fetchJson($this->createParams($location));

        if ( ! $result ) return [];

        $bounds = $result->geometry->viewport;

        return [
            'latitude' => array_sort([$bounds->northeast->lat, $bounds->southwest->lat ]),
            'longitude' => array_sort([$bounds->northeast->lng, $bounds->southwest->lng ])
        ];
    }

    public function getCoordinates($location)
    {
        $result = $this->fetchJson($this->createParams($location));

        if ( ! $result ) return [];

        $location = $result->geometry->location;

        return [
            'latitude' => $location->lat,
            'longitude' => $location->lng,
        ];
    }

    private function fetchJson($data)
    {
        $data['key'] = config('services.google_geocode.api_key');

        $url = $this->url . http_build_query($data);

        return \Cache::rememberForever($url, function() use ($url)
        {
            $this->client = new Client();
            $result = $this->client->get($url);

            return json_decode($result->getBody()->getContents())->results[0] ?? null;
        });
    }

    /**
     * @param array|string $param
     * @return array
     */
    private function createParams($param)
    {
        return is_array($param) ?
            ['latlng' => $param[0] . ',' . $param[1]] :
            ['address' => $param];
    }
}