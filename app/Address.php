<?php

namespace App;

use App\Events\AddressSaved;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $dispatchesEvents = [
        'saved' => AddressSaved::class,
    ];

    public $fillable = [
        'line_1',
        'line_2',
        'state',
        'city',
        'postcode',
    ];

    public $timestamps = false;

    public function stores()
    {
        return $this->hasMany(Store::class);
    }
}
