<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TransactionRepository;

class LocationRevenueController extends Controller
{
    /**
     * @param Request $request
     * @param TransactionRepository $transactionRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, TransactionRepository $transactionRepository)
    {
        if ( ! $request->filled('location') )
            abort(422, 'Please provide a query');

        $amount = $transactionRepository->getTransactionsAmountByLocation($request->location);

        return response()->json(['amount' => number_format($amount / 100, 2)]);
    }
}
