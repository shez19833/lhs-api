<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransactionRequest;
use App\Repositories\TransactionRepository;

class TransactionController extends Controller
{
    /**
     * @param TransactionRequest $request
     * @param TransactionRepository $repository
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TransactionRequest $request, TransactionRepository $repository)
    {
        try {
           $repository->create($request->all());
        } catch (\Exception $e) {
            return response()->json(['message' => 'something went wrong'], 422);
        }

        return response()->json([], 201);
    }
}
