<?php

namespace App\Http\Controllers;

use App\Status;
use App\Transaction;
use App\TransactionHistory;

class TransactionRefundController extends Controller
{
    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(int $id)
    {
        $transaction = Transaction::with('status')->findOrFail($id);

        if ( $transaction->status->status_id === Status::REFUNDED ) {
            return response()->json(['message' => 'Already Refunded'], 422);
        }

        try {
            TransactionHistory::create([
                'transaction_id' => $id,
                'status_id' => Status::REFUNDED,
            ]);
        } catch (\Exception $e) {
            \Log::error('something went wrong' . $e->getMessage());

            return response()->json(['message' => 'something went wrong'], 422);
        }

        return response()->json([], 200);
    }
}
