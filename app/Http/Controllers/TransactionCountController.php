<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TransactionRepository;

class TransactionCountController extends Controller
{
    /**
     * @param Request $request
     * @param TransactionRepository $transactionRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, TransactionRepository $transactionRepository)
    {
        if ( ! $request->filled('location') )
            abort(422, 'Please provide a query');

        $count = $transactionRepository->getTransactionsCountByLocation($request->location);

        return response()->json(['count' => $count]);
    }
}
