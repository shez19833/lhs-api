<?php

namespace App\Repositories;

use App\Status;
use App\Transaction;
use App\Services\Geocoding\GeocodeInterface;

class TransactionRepository {

    private $geocode;

    /**
     * TransactionRepository constructor.
     * @param GeocodeInterface $geocode
     */
    public function __construct(GeocodeInterface $geocode)
    {
        $this->geocode = $geocode;
    }

    /**
     * @param $data
     *
     * @return Transaction
     * @throws \Exception
     */
    public function create($data)
    {
        try {
            $transaction = Transaction::create([
                'id' => $data['id'],
                'store_id' => $data['store_id'],
                'amount' => $data['amount'], // TODO assuming amount is always in pounds eg 47£ or £23.27
                'currency' => $data['currency'],
                'created_at' => $data['created_at'],
            ]);
        } catch (\Exception $e) {
            \Log::error('Error while creating Transaction: ' . $e->getMessage());
            throw($e);
        }

        return $transaction;
    }

    /**
     * @param $location
     * @return int
     */
    public function getTransactionsCountByLocation($location)
    {
        return $this->getTransactionsByLocation($location)->count() ?? 0;
    }

    /**
     * @param $location
     * @return mixed
     */
    public function getTransactionsAmountByLocation($location)
    {
        $transaction = $this->getTransactionsByLocation($location);

        return $transaction ?
            $transaction
                ->where('status.status_id', Status::COMPLETED)
                ->sum('amount') :
            0;
    }

    /**
     * @param $location
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getTransactionsByLocation($location)
    {
        $boundaries = $this->geocode->getBoundaries($location);

        if ( count($boundaries) > 0 )
        {
            return Transaction::with('status')->whereHas('store.address', function ($q) use ($boundaries)
            {
                $q->whereBetween('latitude', $boundaries['latitude'])
                    ->whereBetween('longitude', $boundaries['longitude']);

            })->get();
        }

        return null;
    }

}