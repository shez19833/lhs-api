<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    public $fillable = [
        'name',
        'address_id',
    ];

    public $timestamps = false;

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
