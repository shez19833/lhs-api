<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    public $dates = [
        'created_at',
    ];

    public $fillable = [
        'transaction_id',
        'status_id',
        'created_at',
    ];

    public $table = 'transaction_history';
    public $timestamps = false;


    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            if ( ! $model->created_at ) {
                $model->created_at = Carbon::now();
            }
        });
    }
}
