<?php

namespace App\Listeners;

use App\Address;
use App\Events\AddressSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Geocoding\GeocodeInterface;

class GetCoordinatesForAddress implements ShouldQueue
{
    private $geocode;
    /**
     * Create the event listener.
     *
     * @param GeocodeInterface $geocode
     */
    public function __construct(GeocodeInterface $geocode)
    {
        $this->geocode = $geocode;
    }

    /**
     * Handle the event.
     *
     * @param AddressSaved $event
     * @return void
     */
    public function handle(AddressSaved $event)
    {
        $address = Address::find($event->address_id);

        if ( ! $address->latitude ) {
            $coordinates = $this->geocode->getCoordinates($address->postcode);

            if ($coordinates) {
                $address->latitude = $coordinates['latitude'];
                $address->longitude = $coordinates['longitude'];
                $address->save();
            }
        }
    }
}
