<?php

namespace App\Events;

use App\Address;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddressSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $address_id;

    /**
     * Create a new event instance.
     *
     * @param Address $address
     */
    public function __construct(Address $address)
    {
        $this->address_id = $address->id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
