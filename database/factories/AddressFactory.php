<?php

use Faker\Generator as Faker;

$factory->define(\App\Address::class, function (Faker $faker) {
    return [
        'line_1' => $faker->streetName,
        'line_2' => null,
        'state' => null,
        'city' => 'London',
        'postcode' => $faker->randomElement([
            'W1F 8ZA',
            'W1F 7LW',
            'W1J 9BR',
            'TW9 1AB',
            'TW9 1JY',
            'TW9 1TW',
        ]),
        'latitude' => null, //$faker->latitude,
        'longitude' => null, //$faker->longitude,
    ];
});