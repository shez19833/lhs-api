<?php

use Faker\Generator as Faker;

$factory->define(\App\Transaction::class, function (Faker $faker) {
    return [
        'id' => random_int(1, 9999999),
        'store_id' => function() {
            return factory(\App\Store::class)->create()->id;
        },
        'amount' => random_int(1000,5000),
        'currency' => 'GBP',
        'created_at' => $faker->dateTime(),
    ];
});
