<?php

use Faker\Generator as Faker;

$factory->define(\App\Store::class, function (Faker $faker) {
    return [
        'name' => $faker->streetName,
        'address_id' => function() {
            return factory(\App\Address::class)->create()->id;
        },
    ];
});
