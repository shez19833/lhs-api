<?php

use Faker\Generator as Faker;

$factory->define(\App\TransactionHistory::class, function (Faker $faker) {
    return [
        'transaction_id' => function() {
            return factory(\App\Transaction::class)->create()->id;
        },
        'status_id' => function() {
            return factory(\App\Status::class)->create()->id;
        },
        'created_at' => $faker->dateTime(),
    ];
});
