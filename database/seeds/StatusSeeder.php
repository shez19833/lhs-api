<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['Completed', 'Refunded'] as $status) {
            factory(\App\Status::class)->create([
                'name' => $status
            ]);
        }
    }
}
