<?php

use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Store::all()->each(function($store){
            factory(\App\Transaction::class, random_int(10, 20))->create([
                'store_id' => $store->id,
            ]);
        });
    }
}
