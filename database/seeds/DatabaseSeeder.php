<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StatusSeeder::class);
        $this->call(StoreSeeder::class);
        $this->call(TransactionSeeder::class);
        $this->call(TransactionHistorySeeder::class);
    }
}
