<?php

use Illuminate\Database\Seeder;

class TransactionHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Transaction::all()->each(function ($transaction)
        {
            factory(\App\TransactionHistory::class)->create([
                'status_id' => \App\Status::COMPLETED,
                'transaction_id' => $transaction->id,
            ]);

            if ( random_int(1, 5) === 5) {
                factory(\App\TransactionHistory::class)->create([
                    'status_id' => \App\Status::REFUNDED,
                    'transaction_id' => $transaction->id,
                ]);
            }
        });
    }
}
