<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Transaction;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImportBatchTransactionsTest extends TestCase
{
    use DatabaseTransactions;

    private $transactions;

    public function setUp()
    {
        parent::setUp();

        $this->transactions = factory(Transaction::class, 5)->make();
        $this->createDummyFile();
    }

    public function test_i_can_batch_import_data()
    {
        $this->artisan('system:batch-import');

        $this->assertEquals(
            $this->transactions->count(),
            Transaction::whereIn('id', $this->transactions->pluck('id')->toArray())->count()
        );
    }

    public function test_i_can_batch_import_data_by_specifying_file_path()
    {
        $this->artisan('system:batch-import', ['file_path' => config('batch.file_path')]);

        $this->assertEquals(
            $this->transactions->count(),
            Transaction::whereIn('id', $this->transactions->pluck('id')->toArray())->count()
        );
    }

    public function test_i_cant_batch_import_data_with_incorrect_store_id()
    {
        $this->transactions = factory(Transaction::class, 5)->make([
            'store_id' => random_int(999, 99999),
        ]);
        $this->createDummyFile();

        $this->artisan('system:batch-import');

        $this->assertEquals(
            0,
            Transaction::whereIn('id', $this->transactions->pluck('id')->toArray())->count()
        );
    }

    public function test_i_cant_import_same_data_again()
    {
        $this->artisan('system:batch-import');
        $this->artisan('system:batch-import');

        $this->assertEquals(
            5,
            Transaction::whereIn('id', $this->transactions->pluck('id')->toArray())->count()
        );
    }

    private function createDummyFile()
    {
        $file_path = config('batch.file_path');
        Storage::delete($file_path);

        $headers = ['STORE ID', 'TRANSACTION ID', 'TOTAL AMOUNT', 'CURRENCY', 'CREATED AT'];

        Storage::put($file_path, implode(",", $headers));

        foreach ($this->transactions as $transaction) {
            $data = [
                'store_id' => $transaction->store_id,
                'id' => $transaction->id,
                'amount' => $transaction->amount,
                'currency' => $transaction->currency,
                'created_at' => $transaction->created_at->format('d/M/Y H:i:s'),
            ];
            Storage::append($file_path, implode(",", $data));
        }
    }
}
