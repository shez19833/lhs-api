<?php

namespace Tests\Unit;

use App\Status;
use App\Transaction;
use App\TransactionHistory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    use DatabaseTransactions;

    private $transaction;
    private $post_data;

    public function setUp()
    {
        parent::setUp();

        $this->transaction = factory(Transaction::class)->make();

        $this->post_data = [
            'id' => $this->transaction->id,
            'store_id' => $this->transaction->store_id,
            'amount' => $this->transaction->amount / 100,
            'currency' => $this->transaction->currency,
            'created_at' => (string) $this->transaction->created_at,
        ];
    }

    public function test_i_can_create_a_transaction()
    {
        $this->postJson('transactions', $this->post_data)
            ->assertStatus(201);

        $this->assertTrue(
            Transaction::whereAmount($this->transaction->amount)
                ->whereStoreId($this->transaction->store_id)
                ->whereCurrency($this->transaction->currency)
                ->whereCreatedAt($this->transaction->created_at)
                ->exists()
        );
    }

    public function test_creating_a_transaction_adds_status()
    {
        $this->postJson('transactions', $this->post_data)
            ->assertStatus(201);

        $transaction = Transaction::whereAmount($this->transaction->amount)
            ->whereStoreId($this->transaction->store_id)
            ->whereCurrency($this->transaction->currency)
            ->whereCreatedAt($this->transaction->created_at)
            ->first();

        $this->assertTrue(
            TransactionHistory::whereStatusId(Status::COMPLETED)
                ->whereTransactionId($transaction->id)
                ->exists()
        );
    }

    public function test_currency_is_in_upper_case()
    {
        $data = array_merge($this->post_data, ['currency' => 'gbp']);

        $this->postJson('transactions', $data)
            ->assertStatus(201);

        $this->assertTrue(
            Transaction::whereAmount($this->transaction->amount)
                ->whereStoreId($this->transaction->store_id)
                ->whereCurrency('GBP')
                ->whereCreatedAt($this->transaction->created_at)
                ->exists()
        );
    }

    public function test_i_cant_create_a_transaction_if_no_data_passed()
    {
        $this->postJson('transactions', [])
            ->assertStatus(422);
    }

    public function test_i_cant_create_a_transaction_if_store_doesnt_exist()
    {
        $data = array_merge($this->post_data, ['store_id' => 999999]);

        $this->postJson('transactions', $data)
            ->assertStatus(422);
    }


    // TODO you could have one for currency not valid
}
