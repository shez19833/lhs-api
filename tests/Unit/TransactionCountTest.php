<?php

namespace Tests\Unit;

use App\Status;
use App\Store;
use Mockery;
use App\Address;
use App\Services\Geocoding\Google;
use Tests\TestCase;
use App\Transaction;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TransactionCountTest extends TestCase
{
    use DatabaseTransactions;

    private $mock;
    private $address;

    public function setUp()
    {
        parent::setUp();

        $this->mock = $this->mock(Google::class);

        $this->address = factory(Address::class)->create([
            'postcode' => 'TW9 1AB',
            'latitude' => '51.4606104197085',
            'longitude' => '-0.303467980291502',
        ]);

        $store = factory(Store::class)->create([
            'address_id' => $this->address->id,
        ]);

        factory(Transaction::class, random_int(4,10))->create([
            'store_id' => $store->id
        ]);
    }

    public function mock($class)
    {
        $mock = Mockery::mock($class);

        $this->app->instance($class, $mock);

        return $mock;
    }

    public function test_i_can_get_a_count_of_transaction_given_postcode()
    {
        $this->mock->shouldReceive('getBoundaries')
            ->with($this->address->postcode)
            ->once()
            ->andReturn([
                'latitude' => array_sort(['51.4596104197085', '51.4623083802915' ]),
                'longitude' => array_sort(['-0.304467980291502', '-0.3017700197084979' ])
            ]);

        $count = Transaction::whereHas('store.address', function ($q)
        {
            $q->wherePostcode($this->address->postcode);
        })->count();

        $this->getJson(sprintf('location/transactions/count?location=%s', $this->address->postcode))
            ->assertStatus(200)
            ->assertJson(['count' => $count]);
    }

    public function test_i_can_get_a_count_of_transaction_given_location()
    {
       $this->mock->shouldReceive('getBoundaries')
            ->with($this->address->city)
            ->once()
            ->andReturn([
                'latitude' => array_sort(['51.6723432', '51.38494009999999' ]),
                'longitude' => array_sort(['0.148271', '-0.3514683' ])
            ]);

        $count = Transaction::whereHas('store.address', function ($q)
        {
            $q->whereCity($this->address->city);
        })->count();

        $this->getJson(sprintf('location/transactions/count?location=%s', $this->address->city))
            ->assertStatus(200)
            ->assertJson(['count' => $count]);
    }

    public function test_i_get_an_error_if_location_is_not_provided()
    {
        $this->getJson('location/transactions/count')
            ->assertStatus(422);
    }

    public function tearDown()
    {
        Mockery::close();
    }
}
