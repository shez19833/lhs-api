<?php

namespace Tests\Unit;

use App\Store;
use Mockery;
use App\Status;
use App\Address;
use Tests\TestCase;
use App\Transaction;
use App\Services\Geocoding\Google;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LocationRevenueTest extends TestCase
{
    use DatabaseTransactions;

    private $mock;
    private $address;

    public function setUp()
    {
        parent::setUp();

        $this->mock = $this->mock(Google::class);

        $this->address = factory(Address::class)->create([
            'postcode' => 'TW9 1AB',
            'latitude' => '51.4606104197085',
            'longitude' => '-0.303467980291502',
        ]);

        $store = factory(Store::class)->create([
            'address_id' => $this->address->id,
        ]);

        factory(Transaction::class, random_int(4,10))->create([
            'store_id' => $store->id
        ]);
    }

    public function mock($class)
    {
        $mock = Mockery::mock($class);

        $this->app->instance($class, $mock);

        return $mock;
    }

    public function test_i_can_get_a_total_revenue_given_postcode()
    {
        $this->mock->shouldReceive('getBoundaries')
            ->with($this->address->postcode)
            ->once()
            ->andReturn([
                'latitude' => array_sort(['51.4596104197085', '51.4623083802915' ]),
                'longitude' => array_sort(['-0.304467980291502', '-0.3017700197084979' ])
            ]);


        $transactions = Transaction::whereHas('store.address', function ($q)
        {
            $q->wherePostcode($this->address->postcode);
        })->with('status')->get();

        $amount = $transactions->where('status.status_id', Status::COMPLETED)->sum('amount');


        $this->getJson(sprintf('location/revenue?location=%s', $this->address->postcode))
            ->assertStatus(200)
            ->assertJson(['amount' => number_format($amount / 100, 2)]);
    }

    public function test_i_can_get_a_total_revenue_given_location()
    {
        $this->mock->shouldReceive('getBoundaries')
            ->with($this->address->city)
            ->once()
            ->andReturn([
                'latitude' => array_sort(['51.6723432', '51.38494009999999' ]),
                'longitude' => array_sort(['0.148271', '-0.3514683' ])
            ]);

        $transactions = Transaction::whereHas('store.address', function ($q)
        {
            $q->whereCity($this->address->city);
        })->with('status')->get();

        $amount = $transactions->where('status.status_id', Status::COMPLETED)->sum('amount');

        $this->getJson(sprintf('location/revenue?location=%s', $this->address->city))
            ->assertStatus(200)
            ->assertJson(['amount' => number_format($amount / 100, 2)]);
    }

    public function tearDown()
    {
        Mockery::close();
    }
}
