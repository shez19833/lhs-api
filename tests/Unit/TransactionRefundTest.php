<?php

namespace Tests\Unit;

use App\Status;
use App\Transaction;
use App\TransactionHistory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransactionRefundTest extends TestCase
{
    use DatabaseTransactions;

    private $transaction;
    private $url = 'transactions/%d/refund';

    public function setUp()
    {
        parent::setUp();

        $this->transaction = factory(Transaction::class)->create();

        $this->url = sprintf($this->url, $this->transaction->id);
    }

    public function test_i_can_refund_a_transaction()
    {
        $this->deleteJson($this->url)
            ->assertStatus(200);

        $this->assertTrue(
            TransactionHistory::whereTransactionId($this->transaction->id)
                ->whereStatusId(Status::REFUNDED)
                ->exists()
        );
    }

    public function test_i_cant_refund_if_already_refunded()
    {
        $this->deleteJson($this->url)
            ->assertStatus(200);

        $this->deleteJson($this->url)
            ->assertStatus(422);

        $this->assertEquals(1,
            TransactionHistory::whereTransactionId($this->transaction->id)
                ->whereStatusId(Status::REFUNDED)
                ->count()
        );
    }

    public function test_i_cant_refund_a_transaction_if_invalid_id()
    {
        $url = preg_replace('~\d+~', 10000, $this->url);
        $this->deleteJson($url)
            ->assertStatus(404);

        $this->assertFalse(
            TransactionHistory::whereTransactionId($this->transaction->id)
                ->whereStatusId(Status::REFUNDED)
                ->exists()
        );
    }

}
